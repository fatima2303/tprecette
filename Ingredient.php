<?php

class Ingredient
{
    private $nom;
    private $quantite;
    private $unite;

    public function __construct($nom, $quantite, $unite)
    {
        $this->setNom($nom);
        $this->setQuantite($quantite);
        $this->setUnite($unite);
    }

    function getNom()
    {
        return $this->nom;
    }

    function getQuantite()
    {
        return $this->quantite;
    }

    function getUnite()
    {
        return $this->unite;
    }

    function setNom(string $val)
    {
        $this->nom = $val;
    }

    function setQuantite($val)
    {
        $this->quantite = $val;
    }

    function setUnite(string $val)
    {
        $this->unite = $val;
    }
}
