<?php



class Ustensile{
private $nom;
private $quantite;
private $valeur;

    public function __construct($nom, $quantite = 1, $valeur = null){
        $this->setNom($nom);
        $this->setQuantite($quantite);
        $this->setValeur($valeur);
    }

    function getNom(){
        return $this->nom;
    }

    function getQuantite(){
        return $this->quantite;
    }
    function getValeur(){
        return $this->valeur;
    }

    function setNom(string $val){
        $this->nom = $val;
    }

    function setQuantite($val){
        $this->quantite = $val;
    }

    function setValeur($val){
        $this->valeur = $val;
    }
}
