<?php

class Outil
{
    private $nom;
    private $valeur;

    public function __construct($nom, $valeur = null)
    {
        $this->setNom($nom);
        $this->setValeur($valeur);
    }

    function getNom()
    {
        return $this->nom;
    }

    function getValeur()
    {
        return $this->valeur;
    }

    function setNom(string $val)
    {
        $this->nom = $val;
    }

    function setValeur($val)
    {
        $this->valeur = $val;
    }
}
